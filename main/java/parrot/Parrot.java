package parrot;

public class Parrot {

    private ParrotTypeEnum type;
    private int numberOfCoconuts = 0;
    private double voltage;
    private boolean isNailed;


    public Parrot(ParrotTypeEnum _type, int numberOfCoconuts, double voltage, boolean isNailed) {
        this.type = _type;
        this.numberOfCoconuts = numberOfCoconuts;
        this.voltage = voltage;
        this.isNailed = isNailed;
    }

    public double getSpeed() {
        ParrotSpeedCalculator parrotSpeedCalculator;
        switch(type) {
            case EUROPEAN:
                parrotSpeedCalculator = new ParrotSpeedCalculatorEuropean();
                break;
            case AFRICAN:
                parrotSpeedCalculator = new ParrotSpeedCalculatorAfrican(numberOfCoconuts);
                break;
            case NORWEGIAN_BLUE:
                parrotSpeedCalculator = new ParrotSpeedCalculatorNorwegianBlue(isNailed, voltage);
                break;
            default:
                throw new RuntimeException("Should be unreachable");
        }
        return parrotSpeedCalculator.getSpeed();
    }

}
