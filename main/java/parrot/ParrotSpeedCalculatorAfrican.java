package parrot;

import static parrot.ParrotSpeedCalculatorUtils.*;

public class ParrotSpeedCalculatorAfrican implements ParrotSpeedCalculator {


    private int numberOfCoconuts;

    public ParrotSpeedCalculatorAfrican(int numberOfCoconuts) {
        this.numberOfCoconuts = numberOfCoconuts;
    }

    @Override
    public double getSpeed() {
        return getMaxSpeed();
    }

    private double getMaxSpeed() {
        return Math.max(NONE_SPEED, BASE_SPEED - LOAD_FACTOR * numberOfCoconuts);
    }
}
