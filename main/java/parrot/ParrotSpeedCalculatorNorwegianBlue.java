package parrot;

import static parrot.ParrotSpeedCalculatorUtils.*;

public class ParrotSpeedCalculatorNorwegianBlue implements ParrotSpeedCalculator {

    private boolean isNailed;
    private double voltage;

    public ParrotSpeedCalculatorNorwegianBlue(boolean isNailed, double voltage) {
        this.isNailed = isNailed;
        this.voltage = voltage;
    }

    @Override
    public double getSpeed() {
        return (isNailed) ? NONE_SPEED : getMinSpeed();
    }

    private double getMinSpeed() {
        return Math.min(MAX_SPEED, voltage * BASE_SPEED);
    }
}