package parrot;

public class ParrotSpeedCalculatorUtils {

    public static final double BASE_SPEED = 12.0;
    public static final double LOAD_FACTOR = 9.0;
    public static final double NONE_SPEED = 0;
    public static final double MAX_SPEED = 24.0;

}
