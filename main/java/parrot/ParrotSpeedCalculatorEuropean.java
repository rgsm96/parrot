package parrot;

import static parrot.ParrotSpeedCalculatorUtils.BASE_SPEED;

public class ParrotSpeedCalculatorEuropean implements ParrotSpeedCalculator {


    public ParrotSpeedCalculatorEuropean() {}

    @Override
    public double getSpeed() {
        return BASE_SPEED;
    }
}
