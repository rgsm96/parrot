package parrot;

public interface ParrotSpeedCalculator {
    double getSpeed();
}
